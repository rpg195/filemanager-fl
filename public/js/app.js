toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-bottom-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};

function replaceFileList(filelist){
    var file_sec = $("#filelist");
    file_sec.empty();
    window.currentdir = filelist[0].currentdir;
    $('tr#currdir').replaceWith("<tr id='currdir'><td colspan='6'>Текущая директория: <span id='dir'>"+filelist[0].currentdir+"</span></td></tr>");
    if(filelist[0].currentdir !== './' && filelist[0].currentdir !== "/") {
        var file_line =
            "<tr>" +
            "<td colspan='6' data-sorter='false'><p data-action='updir' onclick='updir($(this))'><i class='fas fa-level-up-alt'></i> Подняться на уровень выше</td>" +
            "</tr>"
        file_sec.append(file_line);
    }
    if(Object.keys(filelist).length >= 2){
        var id = 0;
        $.each(filelist, function (a, b) {
            if (b['name'] === '.' || b['name'] == '..' || b['currentdir']) {
                return true;
            }
            id = id+1
            var filesize = "";
            let reloadInodes = "";
            switch (b['type']) {
                case 'dir':
                    var icon = "<i class='far fa-folder' style='color: rgb(189, 78, 40)'></i>";
                    var name = "<p onclick='chdir($(this))' data-type='dir' data-action='chdir' data-name='" + b['name'] + "'>" + icon + " " + b['name'] + "</p>";
                    reloadInodes = "<i class='fas fa-info' id='open_inodes_modal' data-name='" + b['name'] + "'></i>";
                    break;
                case 'file':
                    var fileexstension = getfileextension(b['name']);
                    switch (fileexstension) {
                        case 'html':
                        case 'htm':
                            var icon = "<i class='fab fa-html5' style='color: red;'></i>";
                            break;
                        case 'js':
                            var icon = "<i class='fab fa-js'></i>";
                            break;
                        case 'php':
                            var icon = "<i class='fab fa-php' style='color: blue;'></i>";
                            break;
                        case 'image':
                        case 'svg':
                            var icon = "<i class='far fa-file-image'></i>";
                            break;
                        case 'archive':
                            var icon = "<i class='far fa-file-archive' style='color: rgb(189, 78, 40)'></i>";
                            break;
                        case 'css':
                            var icon = "<i class='fab fa-css3-alt' style='color: blue;'></i>";
                            break;
                        case 'xml':
                            var icon = "<i class='fas fa-code' style='color: orange;'></i>";
                            break;
                        default:
                            var icon = "<i class='far fa-file-alt'></i>";
                            break;
                    }
                    var name = "<p onclick='openfile($(this));' data-type='file' data-action='open-file' data-name='" + b['name'] + "'>" + icon + " " + b['name'] + "</p>";
                    var filesize = b['size'];
                    break;
                default:
                    var icon = "";
                    break;

            }

            var file_line =
                "<tr>" +
                "<td><input type='checkbox' id='checkbox_"+id+"' class='checkbox' data-name='"+b['name']+"' data-type='"+b['type']+"'></td>" +
                "<td>" + name + "</td>" +
                "<td><i class='fas fa-cog' class='settings' id='file_settings' data-chmod='"+b["chmod_int"]+"' data-name='"+b['name']+"' data-type='"+b['type']+"' data-tooltipe='yes' data-placement='bottom' title='Дополнительно'></i></td>" +
                "<td>"+reloadInodes+"</td>" +
                "<td>" + b["chmod_int"] + "</td>" +
                "<td>" + b['modify_c'] + "</td>" +
                "<td>" + filesize + "</td>" +
                "</tr>"
            file_sec.append(file_line);
        })
    } else {
        var file_line =
            "<tr>" +
            "<td colspan='6'><b><center>Файлов нет</center></b></td>" +
            "</tr>"
        file_sec.append(file_line);
    }
    $(".fileListContent").trigger("update");
    sort();
    disabledButtons();
    $('table#fileListContent thead').find("input[type=checkbox]").prop('checked',false);

};

function sort() {
    $(".fileListContent").tablesorter({
        sortForce: [[0,0]],
        headers: {
            0: {
                sorter: false,
            },
            2: {
                sorter: false
            },
        },
        cssDesc: "sortDesc",
        cssAsc: "sortAsc",
    }).trigger('update');;
}

function updir(e) {
    var _this = e;
    var action = e.attr('data-action');
    loader = $("#fileListLoader");
    body = $("#fileListContent");
    loader.show();
    body.hide();
    if(action == 'updir') {
        $.post('/UpDir', {
            action: action ,
        }, function(result){
            var data = $.parseJSON(result);
            if(data.status === 'ok') {
                $.post('/GetFiles', {
                }, function(result){
                    var data = $.parseJSON(result);
                    replaceFileList(data);
                    loader.hide();
                    body.show();
                });
            }
        });
    } else {
        return false;
    }
}

function openfile(e) {
    var filename = e.attr('data-name');
    var modalLoader = $("#fileEditorLoader");
    var modalBody = $("#fileEditor");
    var modalFooter = $("#fileEditorFooter");
    var modalTitle = $("#fileEditorTitle > small");
    var modal = $("#modal_fileeditor");
    $("#customFileEncoding").attr('data-name',filename);
    $("#fileEditorSave").attr('data-filename',filename);
    modalTitle.text("Редактирование файла "+window.currentdir+filename);
    var acemode = "ace/mode/";
    var fileextension = getfileextension(filename);
    if(fileextension === 'unknown') {
        fileextension = 'text';
    }
    var mode = acemode+fileextension;
    window.aceeditor = ace.edit("fileEditor");
    ace.require("ace/ext/language_tools");
    window.aceeditor.setOptions({
        autoScrollEditorIntoView: true,
        copyWithEmptySelection: true,
        showLineNumbers: true,
        enableSnippets: true,
        enableLiveAutocompletion: true,
        enableBasicAutocompletion: true,
        mergeUndoDeltas: "always",
    });
    window.aceeditor.session.setMode(mode);
    window.aceeditor.setTheme("ace/theme/eclipse");
    $(modal).modal("show");
    modalLoader.show();
    modalBody.hide();
    modalFooter.hide();
    modalTitle.hide();

    $.ajax({
        type: "POST",
        url: "/GetFileContent",
        data: "filename="+filename,
        success: function(result){
            var json = JSON.parse(result);
            switch (json.status) {
                case 'unsupported_file_extension':
                    modalLoader.hide();
                    modalBody.hide();
                    modalFooter.hide();
                    modalTitle.hide();
                    setTimeout(function (){
                        modal.modal("hide");
                    },300);
                    toastr["error"]("Файлы с расширением "+getfileextension(filename,true)+" не поддерживается для редактирования ","Внимание!");
                    break;
                default:
                    window.aceeditor.setValue(json.content,1);
                    modalLoader.hide();
                    modalBody.show();
                    modalFooter.show();
                    modalTitle.show();

                    break;
            };
        }
    });
}
function chdir(e) {
    var _this = e;
    var action = e.attr('data-action');
    var newdir = e.attr('data-name')
    loader = $("#fileListLoader");
    body = $("#fileListContent");
    loader.show();
    body.hide();

    if(action == 'chdir') {
        $.post('/GetFiles', {
            dir: newdir,
        }, function(result){
            var data = $.parseJSON(result);
            replaceFileList(data);
            loader.hide();
            body.show();
        });
    } else {
        return;
    }
}

function getfileextension(file,original = false) {
    var extension = file.substr( (file.lastIndexOf('.') +1) );
    if(original) {
        return extension;
    }
    switch(extension) {
        case 'jpg':
        case 'png':
        case 'gif':
            return "image";
        case 'zip':
        case 'rar':
        case '7z':
        case 'gz':
        case 'bz2':
        case 'xz':
        case 'tar':
        case 'tgz':
            return "archive";
        case 'pdf':
            return "pdf";
        case 'php':
            return "php";
        case 'svg':
            return "svg";
        case 'css':
            return 'css';
        case 'js':
            return 'js';
        case 'html':
            return 'html';
        case 'ini':
            return 'ini';
        case "sql":
            return "mysql";
        case "xml":
            return "xml";
        default:
            return "unknown";
            break;

    }
}

function updateFileList() {
    loader = $("#fileListLoader");
    body = $("#fileListContent");
    loader.show();
    body.hide();
    $.post('/GetFiles', {
    }, function(result){
        var data = $.parseJSON(result);
        replaceFileList(data);
        loader.hide();
        body.show();
        toastr["success"]("", "Список файлов обновлен");
    });

};

function getChecked() {
    var checked = [];
    $('table tbody input:checked').each(function() {
        checked.push($(this).attr('data-name'));
    });
    return checked;
}
function customconfirm (confirmtext, callback) {
    var modal = $("#modal_confirm");
    var title = $("#confirm-text");
    title.html(confirmtext);
    modal.modal("show");
    $(modal).unbind().modal();

    $('#btn-confirm-yes').unbind().on('click', function () {
        callback(true);
        setTimeout(function() {
            modal.modal('hide');
        }, 300);
        return;
    });

    $('#btn-confirm-no').unbind().on('click', function () {
        callback(false);
        setTimeout(function() {
            modal.modal('hide');
        }, 300);
        return;
    });
};

function download() {
    loader = $("#fileListLoader");
    body = $("#fileListContent");
    loader.show();
    body.hide();
    var checked = getChecked();
    if(checked.length < 1) {
        loader.hide();
        body.show();
        toastr["success"]("", "Выберите файлы которые необходимо скачать.");
        return;
    }
    let DDdir = false;
    $.each(checked,function (key,val) {
        val = val.replace(/\./g, '\\.');
       let element = $("[data-name="+val+"]");
       if(element.data('type') == 'dir') {
            let DDmodal = $("#modal_download_dir");
            DDmodal.modal('show');
            DDdir = true;

       }
    });

    if(DDdir) {
        loader.hide();
        body.show();
        return true;
    }

    toastr["success"]("", "Готовим файл для загрузки. Ожидайте начала скачивания.");
    $.ajax({
        type: "POST",
        url: "/download",
        data: {
            files: checked,
        },
        success: function(result){
            loader.hide();
            body.show();
            switch (result) {
                case 'err_cant_make_archive':
                case 'err_cant_change_perm':
                case 'not_pass_all_params':
                    toastr["error"]("Не удалось создать архив, попробуйте еще раз.");
                    break;
                case 'only_files_can_be_download':
                    toastr['error']("Нельзя скачать директорию");
                    break;
                case 'only_one_file_can_be_download':
                    toastr['error']("Только один файл можно скачать за один раз");
                    break;
                default:
                    var a = document.createElement("a");
                    a.href = result;
                    a.setAttribute("download", checked[0]);
                    a.click();
                    // $.fileDownload("/downloads/"+result)
                    //     .done(function () { console.log('success'); })
                    //     .fail(function () { toastr["error"]("Ошибка!", "Попробуйте еще раз."); });
                    break;
            }

        },
        fail: function () {
            toastr["error"]("Ошибка!", "Не удалось создать архив, попробуйте еще раз.");
            return;

        }
    });
}

function checked_chmod(name,perm) {
    //availables chmod 0,1,2,3,4,5,6,7
    // 0 = none, 1 = exec, 2=write, 3=exec+write,4=read,5=exec+read,6=read+write,7=read+write+exec
    var cb = {
        "owner_e_cb": $("#owner-execution-perm"),
        "group_e_cb": $("#group-execution-perm"),
        "other_e_cb": $("#other-execution-perm"),
        "owner_w_cb": $("#owner-write-perm"),
        "group_w_cb": $("#group-write-perm"),
        "other_w_cb": $("#other-write-perm"),
        "owner_r_cb": $("#owner-read-perm"),
        "group_r_cb": $("#group-read-perm"),
        "other_r_cb": $("#other-read-perm"),
    };
    switch (perm) {
        case "1":
            cb[name+"_r_cb"].prop('checked', false);
            cb[name+"_e_cb"].prop('checked', true);
            cb[name+"_w_cb"].prop('checked', false);
            break;
        case "2":
            cb[name+"_r_cb"].prop('checked', false);
            cb[name+"_e_cb"].prop('checked', false);
            cb[name+"_w_cb"].prop('checked', true);
            break;
        case "3":
            cb[name+"_r_cb"].prop('checked', false);
            cb[name+"_e_cb"].prop('checked', true);
            cb[name+"_w_cb"].prop('checked', true);
            break;
        case "4":
            cb[name+"_r_cb"].prop('checked', true);
            cb[name+"_e_cb"].prop('checked', false);
            cb[name+"_w_cb"].prop('checked', false);
            break;
        case "5":
            cb[name+"_r_cb"].prop('checked', true);
            cb[name+"_e_cb"].prop('checked', true);
            cb[name+"_w_cb"].prop('checked', false);
            break;
        case "6":
            cb[name+"_r_cb"].prop('checked', true);
            cb[name+"_e_cb"].prop('checked', false);
            cb[name+"_w_cb"].prop('checked', true);
            break;
        case "7":
            cb[name+"_r_cb"].prop('checked', true);
            cb[name+"_e_cb"].prop('checked', true);
            cb[name+"_w_cb"].prop('checked', true);
            break;
        default:
            cb[name+"_r_cb"].prop('checked', false);
            cb[name+"_e_cb"].prop('checked', false);
            cb[name+"_w_cb"].prop('checked', false);
            break;
    }
}

function disabledButtons() {
    var checked = getChecked();
    var downloadbutton = $('#download-button');
    var removebutton = $('#remove-button');
    var extractzip = $('#extract-zip');
    var copybutton = $('#copy-button');
    var cutbutton = $('#cut-button');
    var pastebutton = $('#paste-button');
    var makearchive = $("#make-archive");
    switch (checked.length) {
        case 0:
            copybutton.attr("disabled", true);
            cutbutton.attr("disabled", true);
            removebutton.attr("disabled", true);
            downloadbutton.attr("disabled", true);
            extractzip.attr("disabled", true);
            makearchive.attr("disabled", true);
            break;
        case 1:
            var ext = getfileextension(checked[0],false);
            if(ext == 'archive') {
                extractzip.attr('disabled', false);
            } else {
                extractzip.attr('disabled', true);
            }
            makearchive.attr("disabled", false);
            copybutton.attr("disabled", false);
            cutbutton.attr("disabled", false);
            removebutton.attr("disabled", false);
            downloadbutton.attr("disabled", false);
            break;
        default:
            copybutton.attr("disabled", false);
            cutbutton.attr("disabled", false);
            removebutton.attr("disabled", false);
            downloadbutton.attr("disabled", false);
            extractzip.attr('disabled', true);
            makearchive.attr("disabled", false);
            break;
    }

    if(checked.length > 1 || checked.length < 1) {
        downloadbutton.attr("disabled", true);
    } else {
        downloadbutton.attr("disabled", false);
    }

    var cutleng = Object.keys(move.cut).length;
    var copyleng = Object.keys(move.copy).length;
    if(copyleng == 0 && cutleng == 0) {
        pastebutton.attr("disabled", true);
    } else {
        pastebutton.attr("disabled", false);
    }
}
var move = {};
var opts = {};
opts.maxfilesize = 209715200;
$(document).ready(function () {
    move.copy = {};
    move.cut = {};
    $(document).on('click','#copy-button',function (e) {
        var currentDir = $("tr#currdir > td > span#dir").html();
        move.copy = {};
        move.cut = {};
        move.copy = getChecked();
        $.each(move.copy, function (key,val) {
            console.log("copy in: "+currentDir);
            move.copy[key] = currentDir+val;
        });
        disabledButtons();

    });
    $(document).on('click','#cut-button',function (e) {
        var currentDir = $("tr#currdir > td > span#dir").html();
        move.copy = {};
        move.cut = {};
        move.cut = getChecked();
        $.each(move.cut, function (key,val) {
            move.cut[key] = currentDir+val;
        });
        disabledButtons();
    });
    $(document).on('click','#paste-button',function (e) {
        var cutleng = Object.keys(move.cut).length;
        var copyleng = Object.keys(move.copy).length;
        if(cutleng == 0 && copyleng == 0) {
            toastr['error']("Для того, что бы вставить нужно что-то скопировать");
            return;
        }
        var action;
        var filelist;
        var ruact;
        var ruactno;
        if(cutleng > 0) {
            action = 'cut';
            filelist = move.cut;
            ruact = 'перемещены';
            ruactno = "переместить";
        } else if(copyleng > 0) {
            action = 'copy';
            filelist = move.copy;
            ruact = 'скопированы';
            ruactno = 'скопировать';

        } else {
            toastr['error']("Неизвестная ошибка, попробуйте еще раз!");
        }
        var confirmtext = "Будут "+ruact+" следующие файлы:<br/>" + filelist;
        customconfirm(confirmtext, function (status) {
            if(status == false) {
                return;
            } else {
                $.ajax({
                    type: "POST",
                    url: "/move",
                    data: {
                        files: filelist,
                        action: action,
                        todir: $("tr#currdir > td > span#dir").html(),
                    },
                    success: function(result) {
                        var json = $.parseJSON(result);

                        if(json.status == 'ok') {
                            toastr["success"]("Успешно "+ruact+".");
                        } else if(json.status == 'err') {
                            toastr["error"]("Не удалось "+ruactno+" следующие файлы: <br/>"+ json.files);
                        }
                        updateFileList();
                        loader.hide();
                        body.show();
                        return;
                    },
                });
                move.copy = {};
                move.cut = {};
            }
        });

        disabledButtons();
    });

    $(document).on('click', "#file_settings", function () {
        $("#recursive_select").hide();
        var _this = $(this);
        var modal = $("#options_modal");
        var file_name = _this.attr('data-name');
        var file_type = _this.attr('data-type');
        var curr_chmod = _this.attr('data-chmod');
        var curr_chmod_arr = curr_chmod.split("");
        if(!file_name || !file_type) {
            toastr["error"]("Ошибка!", "Обновите страницу и попробуйте еще раз.");
            return;
        }
        if(file_type == 'dir') {
            $("#recursive_select").show();
        }
        $("#saveOptions").attr('data-name',file_name);
        $("#saveOptions").attr('data-type',file_type);
        $("#saveOptions").attr('data-chmod',curr_chmod);
        $("#options_body").find('#file_name_new').val(file_name);
        var counter = 1;
        $.each(curr_chmod_arr.slice(1), function (key,val) {
           switch(counter) {
               case 1:
                   checked_chmod('owner',val)
                   break;
               case 2:
                   checked_chmod('group',val)
                   break;
               case 3:
                   checked_chmod('other',val)
                   break;
           }
           counter++;
        });

        modal.modal('show');
    });

    $(document).on('click',"#saveOptions",function () {
        var _this = $(this);
        var modal = $("#options_modal");
        var loader = $("#options_modal").find('.modal-loader');
        var body = $("#options_modal").find('.modal-body');
        var footer = $("#options_modal").find('.modal-footer');
        var file_name = _this.attr('data-name');
        var file_name_new = $('#file_name_new').val();
        var file_type = _this.attr('data-type');
        body.hide();
        footer.hide();
        loader.show();
        var recursive = 'none';
        if(file_type == 'dir') {
            switch ($("#change-perm-type > option:selected").attr('name')) {
                case 'recursive-file':
                    recursive = 'files';
                    break;
                case 'recursive-dir':
                    recursive = 'dirs';
                    break;
                case 'recursive-all':
                    recursive = 'all';
                    break;
                default:
                    recursive = 'none';
                    break;
            }
        }
        var owner_new = 0;
        var group_new = 0;
        var other_new = 0;
        $('tbody#file-permission > tr > td > input:checked').each(function() {
                switch ($(this).attr('data-name')) {
                    case 'owner':
                        owner_new = parseInt(owner_new) + parseInt($(this).val());
                        break;
                    case 'group':
                        group_new = parseInt(group_new) + parseInt($(this).val());
                        break;
                    case 'other':
                        other_new = parseInt(other_new) + parseInt($(this).val());
                        break;
                }

        });
        var chmod_new = owner_new.toString() + group_new.toString() + other_new.toString();
        if(!(file_name || file_name_new || file_type || chmod_new || recursive)) {
            toastr["error"]("Ошибка!", "Обновите страницу и попробуйте еще раз.");
            return;
        }
        $.ajax({
            type: "POST",
            url: "/changeFileOptions",
            data: {
                filename: file_name,
                recursive: recursive,
                chmod: chmod_new,
                filename_new: file_name_new,
            },
            success: function (result) {
                var json = $.parseJSON(result);
                if(json.filename == 'ok' && json.chmod == 'ok') {
                    toastr["success"]("Новые данные успешно сохранены.");
                    modal.modal('hide');
                    body.show();
                    footer.show();
                    loader.hide();
                } else if(json.filename == 'ok' && json.chmod != 'ok') {
                    toastr["error"]("Не удалось установить новые права.");
                    modal.modal('show');
                    body.show();
                    footer.show();
                    loader.hide();
                } else if(json.filename == 'err_has_restricted_chars') {
                    toastr["error"]("В имени файла нельзя использовать символы:  / : * ? \" < > | + ~ ! и пробел");
                    modal.modal('show');
                    body.show();
                    footer.show();
                    loader.hide();
                } else if(json.filename != 'ok' && json.chmod == 'ok') {
                    toastr["error"]("Не удалось изменить имя файла.");
                    modal.modal('show');
                    body.show();
                    footer.show();
                    loader.hide();
                }
            },
        });
    });

    $(document).on('click','#remove-button',function (e) {
        loader = $("#fileListLoader");
        body = $("#fileListContent");
        checked = [];
        var checked = getChecked();
        var confirmText = "Подтвердите удаление файлов: " + checked;
        customconfirm(confirmText,function (status) {
            if(status == false) {
                return;
            }
            $.ajax({
                type: "POST",
                url: "/remove",
                data: {
                    files: checked,
                },
                success: function(result) {
                    if(result == 'success') {
                        if(checked.length > 1) {
                            toastr["success"](checked + " успешно удалены.", "Внимание!");
                        } else {
                            toastr["success"](checked + " успешно удален.", "Внимание!");
                        }
                    } else {
                        toastr["error"](result + " не удалось удалить.", "Внимание!");
                    }
                    updateFileList();
                    loader.hide();
                    body.show();
                    return;
                },
            });
        });
    });



    $(document).on('change', 'input[type="checkbox"]', function () {
        disabledButtons();
    });

    $('.modal').on('hidden.bs.modal', function (e) {
        $(this).find("input[type='text'],textarea,select").val('').end();
        $(this).find("input[type='checkbox']:checked").prop('checked', false);
    });

    $('#modal_fileeditor').on('hidden.bs.modal', function (e) {
        window.aceeditor.destroy();
        window.aceeditor.container.remove()
        var str = '<div class="modal-body" id="fileEditor"></div>';
        $("#fileEditorHeader").after(str);
        $('#customFileEncoding').prop('selectedIndex',0);
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        },
        cache: false

    });

    $(document).on("click","#makeDir",function () {
        var dirname = $("#newdir_modal").find("#dirname").val();
        var modal = $("#newdir_modal");
        var modalBody = $("#newdir_modal").find(".modal-body");
        var modalFooter = $("#newdir_modal").find(".modal-footer");
        var modalLoader = $("#newdir_modal").find(".modal-loader");
        $.ajax({
            type: "POST",
            url: "/makeDir",
            data: {
                dirname: dirname,
            },
            success: function(result){
                if(result === 'dir_already_exists') {
                    toastr["error"]("Внимание!", "Директория уже существует!");
                    return;
                } else if(result === 'err_has_restricted_chars') {
                    toastr["error"]("Внимание!", "В имени директории нельзя использовать символы:  / : * ? \" < > | + ~ ! и пробел");
                    return;
                }
                updateFileList();
                modalLoader.hide();
                modalBody.show();
                modalFooter.show();
                modal.modal("hide");
                toastr["success"]("Внимание!", "Директория успешно создана!");
            },
            error: function(XMLHttpRequest, textStatus, errorThrown, some, some2) {
                toastr["error"]("Внимание!", "Не удалось создать директорию!");
                modalLoader.hide();
                modalBody.show();
                modalFooter.show();
            }
        });
    });

    $(document).on("click","#extract-zip",function () {
        var archive = getChecked();
        var filename = archive[0];
        loader = $("#fileListLoader");
        body = $("#fileListContent");
        if(archive.length > 1 || archive.length < 1) {
            toastr["error"]("Внимание!", "Можно распаковать только один архив за раз!");
            return;
        }
        var ext = getfileextension(filename,false);
        if(ext != 'archive') {

            toastr["error"]("Внимание!", "Можно распаковать только архивы в формате .zip, .rar, .7z, .gz, .bz2, .xz2!");
            return;
        }
        loader.show();
        body.hide();

        $.ajax({
            type: "POST",
            url: "/unzip",
            data: {
                filename: filename,
            },
            success: function(result){
                console.log(result);
                if(result == 'ok') {
                    toastr["success"]("Архив успешно извлечен!");
                    updateFileList();
                    loader.hide();
                    body.show();
                } else {
                    toastr["error"]("Некоторые файлы не были распакованы, так как используют в именах кодировку несовместимую с UNIX-подобными OS!");
                    loader.hide();
                    body.show();
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown, some, some2) {
                toastr["error"]("Не удалось извлечь архив!");
                loader.hide();
                body.show();
            }
        });
    });

    $(document).on("click","#make-archive",function () {
        var archive = getChecked();
        var filename = archive[0];
        loader = $("#fileListLoader");
        body = $("#fileListContent");
        if(archive.length < 1) {
            toastr["error"]("Внимание!", "Файлы не выбраны");
            return;
        }
        loader.show();
        body.hide();

        $.ajax({
            type: "POST",
            url: "/makeArchive",
            data: {
                filelist: getChecked(),
            },
            success: function(result){
                if(result == 'ok') {
                    toastr["success"]("Архив успешно извлечен!");
                    updateFileList();
                    loader.hide();
                    body.show();
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown, some, some2) {
                toastr["error"]("Не удалось извлечь архив!");
                loader.hide();
                body.show();
            }
        });
    });


    $(document).on("click","#makeFile",function () {
        var filename = $("#newfile_modal").find("#filename").val();
        var modal = $("#newfile_modal");
        var modalBody = $("#newfile_modal").find(".modal-body");
        var modalFooter = $("#newfile_modal").find(".modal-footer");
        var modalLoader = $("#newfile_modal").find(".modal-loader");
        $.ajax({
            type: "POST",
            url: "/makeFile",
            data: {
                filename: filename,
            },
            success: function(result){
                if(result === 'file_already_exists') {
                    toastr["error"]("Файл уже существует!");
                    return;
                }else if(result === 'err_has_restricted_chars') {
                    toastr["error"]("В имени файла нельзя использовать символы:  / : * ? \" < > | + ~ ! и пробел");
                    return;
                }
                updateFileList();
                modalLoader.hide();
                modalBody.show();
                modalFooter.show();
                modal.modal("hide");
                toastr["success"]("Файл успешно создан!");
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                toastr["error"]("Не удалось создать файл!");
                modalLoader.hide();
                modalBody.show();
                modalFooter.show();
            }
        });
    });

    $(document).on("click","#fileEditorSave",function () {
        var content = encodeURIComponent(window.aceeditor.session.getValue());
        var filename = $(this).attr('data-filename');
        var modalLoader = $("#fileEditorLoader");
        var modalBody = $("#fileEditor");
        var modalFooter = $("#fileEditorFooter");
        modalLoader.show();
        modalBody.hide();
        $.ajax({
            type: "POST",
            url: "/SaveFile",
            data: {
                filename: filename,
                content: content,
                encoding: $("#customFileEncoding").val(),
            },
            success: function(result){
                if(result === 'ok') {
                    $("#modal_fileeditor").modal("hide");
                    $('#customFileEncoding').prop('selectedIndex', 0);
                    toastr["success"]("Файл успешно сохранен!");
                } else if(result == 'enc_not_supp') {
                    toastr['error']("Переданная кодировка не поддерживается!");
                } else {
                    toastr["error"](result,"Ошибка!");
                }
                modalLoader.hide();
                modalBody.show();
                modalFooter.show();
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                toastr["error"]( "Не удалось сохранить содержимое файла!");
                modalLoader.hide();
                modalBody.show();
                modalFooter.show();
            }
        });

    });

    $(document).on("change","#customFileEncoding",function () {
        var encoding = $("#customFileEncoding option:selected").val();
        var filename = $("#customFileEncoding").attr('data-name');
        var modalLoader = $("#fileEditorLoader");
        var modalBody = $("#fileEditor");
        var modalFooter = $("#fileEditorFooter");
        modalLoader.show();
        modalBody.hide();
        modalFooter.hide();
        $.ajax({
            type: "POST",
            url: "/GetFileContent",
            data: {
                filename: filename,
                customFileEncoding: encoding,
            },
            success: function(result){
                var json = $.parseJSON(result);
                window.aceeditor.setValue(json.content,1);
                modalLoader.hide();
                modalBody.show();
                modalFooter.show();
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                if(textStatus === 'error') {
                    modalLoader.hide();
                    modalBody.hide();
                    modalFooter.hide();
                    toastr["error"]("Не удалось загрузить содержимое файла!");
                    return;
                }

            }
        });

    });
    $('#uploadFileInput').on('change', function() {
        var file = this.files[0];
        if (file.size > opts.maxfilesize) {
            toastr["error"]("Максимальный размер файла для зарузки 200МБ!");
            $("#uploadFileInput").val('');
            return;
        }
    });
    $(document).on("click","#uploadFile",function () {
        var _this = $(this);
        var modal = $("#upload_modal");
        var file = $("#uploadFileInput").get(0).files[0];
        var progg = $('.progress-upload');
        var progress = $('.progress-bar-upload');
        var filesize = file.size;
        if(filesize > opts.maxfilesize) {
            toastr["error"]("Максимальный размер файла для зарузки 200МБ!");
            $("#uploadFileInput").val('');
            return;
        }
        var fd = new FormData();
        fd.append('file',file);
        window.nowuploadfile = 1
        $.ajax({
            xhr: function() {
                var xhr = new window.XMLHttpRequest();

                xhr.upload.addEventListener("progress", function(evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        percentComplete = parseInt(percentComplete * 50);
                        progress.width(percentComplete+"%");
                        progress.html(percentComplete+"%");

                        if (percentComplete === 100) {

                        }

                    }
                }, false);
                xhr.onprogress = function (e){
                    var response = xhr.responseText;
                     var arr = response.split(/[\s,]+/);
                     var percent = arr[arr.length-2];
                     var x = parseInt(percent);
                    var newProgress = x+50;
                    progress.width(newProgress+"%");
                    progress.html(newProgress+"%");
                    if(newProgress === 100) {
                        progress.addClass('bg-success');
                        progress.html('Файл успешно загружен');
                        toastr["success"]("Файл успешно загружен!");
                        modal.modal('hide');
                        $("#uploadFileInput").val('');
                        updateFileList();
                        progress.html("0%");
                        progress.width("0%");
                        progress.width("0%");
                        progg.hide();
                    }
                };
                return xhr;
            },
            type: "POST",
            url: "/uploadFile",
            processData: false,
            contentType: false,
            cache: false,
            data: fd,
            beforeSend: function () {

                if($("#uploadFileInput").get(0).files[0].name.match(/(\\|\/|:|\*|\?|"|<|>|\||\+|\~| )/) !== null){
                    toastr["error"]("Не удалось загрузить файл. Имя файла содержит один или несколько запрещенных символов:  / : * ? \" < > | + ~ ! и пробел");
                    modal.modal('show');
                    body.show();
                    footer.show();
                    loader.hide();
                    return;
                }

                progg.show();
                progress.empty();
                var percentVal = '0%';
                progress.width(percentVal);
                progress.html(percentVal);
            },
            success: function(result){
                if(result == 'err_has_restricted_chars') {
                    toastr["error"]("Не удалось загрузить файл. Имя файла содержит один или несколько запрещенных символов:  / : * ? \" < > | + ~ ! и пробел");
                    modal.modal('show');
                    body.show();
                    footer.show();
                    loader.hide();
                    return;
                }
                $("#upload_modal button").attr("disabled", false);
                window.nowuploadfile = 0;
            },
            error: function(XMLHttpRequest, textStatus, errorThrown, some, some2) {
                window.nowuploadfile = 0;
                $("#upload_modal button").attr("disabled", false);
                toastr["error"]("Не удалось загрузить файл!","Внимание!");
                return;
            }
        });

    });
    updateFileList();

    $('input').attr('autocomplete', 'off');

    $(document).on('click','#selectAll',function(){
       if($(this).is(":checked")) {
           $('table#fileListContent tbody').find("input[type=checkbox]").prop('checked',true);
       } else {
           $('table#fileListContent tbody').find("input[type=checkbox]").prop('checked',false);
       }
    });


    $(document).on('click','#open_inodes_modal',function() {
        let dirname = $(this).attr('data-name');
        let modal = $("#check_inodes_modal")
        modal.modal('show');
        let modalLoader = modal.find(".modal-loader");
        let modalBody = modal.find(".modal-body");
        let modalFooter = modal.find(".modal-footer");
        let innodesSpan = modal.find("span#count_innodes_in_dir");
        let sizeSpan = modal.find("span#directory_size");
        let updateInodesButton = modal.find("#check_inodes");
        updateInodesButton.attr('data-dirname',dirname);

        modalLoader.show();
        modalBody.hide();
        modalFooter.hide();
        innodesSpan.html("");
        sizeSpan.html("");



        $.ajax({
            type: "POST",
            url: "/getLastInodesValue",
            data: "dirname="+dirname,
            success: function(result){
                var json = JSON.parse(result);
                if(json.status == 'success') {
                    innodesSpan.html(json.message.inodes);
                    sizeSpan.html(json.message.size + " Мб");
                }else if(json.message == 'no_data') {
                    innodesSpan.html("Нет данных");
                    sizeSpan.html("Нет данных");
                } else {
                    innodesSpan.html("Нет данных");
                    sizespanSpan.html("Нет данных");
                    toastr["error"]("Произошла ошибка. Попробуйте перезагрузить страницу и попробовать еще раз","Внимание!");
                }
                modalLoader.hide();
                modalBody.show();
                modalFooter.show();
            }
        });

    });

    $(document).on('click',"#check_inodes",function() {
        let dirname = $(this).attr('data-dirname');
        let modal = $("#check_inodes_modal");
        // modal.modal('show');
        let modalLoader = modal.find(".modal-loader");
        let modalBody = modal.find(".modal-body");
        let modalFooter = modal.find(".modal-footer");
        let innodesSpan = modal.find("span#count_innodes_in_dir");
        let sizeSpan = modal.find("span#directory_size");

        modalLoader.show();
        modalBody.hide();
        modalFooter.hide();
        innodesSpan.html("");
        sizeSpan.html("");

        $.ajax({
            type: "POST",
            url: "/checkInodes",
            data: "dirname="+dirname,
            success: function(result){
                var json = JSON.parse(result);
                switch (json.status) {
                    case 'success':
                        innodesSpan.html(json.message.inodes);
                        sizeSpan.html(json.message.size + " Мб");
                        modalLoader.hide();
                        modalBody.show();
                        modalFooter.show();
                        break;
                    default:
                        modalLoader.hide();
                        modalBody.show();
                        modalFooter.show();
                        toastr["error"]("Произошла ошибка. Попробуйте перезагрузить страницу и попробовать еще раз","Внимание!");
                        break;
                }
            }
        });


    })
}); 
if(window.nowuploadfile === 1) {
    window.onbeforeunload = confirmExit;
    function confirmExit() {
        return 'message';
    }
}


$(function () {
    $('[data-tooltipe="yes"]').tooltip();
    
    $(document).on('click','#logout-button',function () {
        customconfirm("Вы действительно желаете выйти?",function(status){
            if(status) {
                window.location.href = "/logout";
            }
        });
    })


}());
