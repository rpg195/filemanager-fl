<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>CityHost FileManager</title>
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link href="/css/app.css" rel="stylesheet" type="text/css">
    <link href="/css/custom.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="container">
    <div class='row'>
        <form class='form-signin offset-sm-3 col-sm-6'  action="/login" method="POST">

            @csrf
            <h2 class='form-signin-heading'>Авторизация</h2>
            @if(isset($errmsg))
                <div class="alert alert-danger" role="alert">
                    Ошибка! {{ $errmsg['errmsg'] }}
                </div>
            @endif
            <div class="form-group">
                <label for='ServerIp'>IP адрес сервера</label>
                <input id='ServerIp' name="ServerIp" class='form-control @if(isset($ServerIp["err"]) && $ServerIp["err"] == 'empty') error @endif' placeholder='127.0.0.1' type='text' value="@if(isset($ServerIp["data"]) && !empty($ServerIp["data"])){{$ServerIp["data"]}}@endif">
            </div>
            <div class="form-group">
                <label for='FtpUsername' class='sr-only'>FTP логин</label>
                <input id='FtpUsername' name="FtpUsername" class='form-control @if(isset($FtpUsername["err"]) and $FtpUsername["err"] == 'empty') error @endif' placeholder='FTP логин' type='text' value="@if(isset($FtpUsername["data"]) && !empty($FtpUsername['data'])){{$FtpUsername["data"]}}@endif">
            </div>
            <div class="form-group">
                <label for='FtpPassword' class='sr-only'>Пароль</label>
                <input id='FtpPassword' name="FtpPassword" class='form-control @if(isset($FtpPassword["err"]) and $FtpPassword["err"] == 'empty') error @endif' placeholder='Пароль' type='password'>
            </div>
            <button class='btn btn-lg btn-primary btn-block' type='submit'>Войти</button>
            </form>
        </div>
</div>
</body>
</html>
