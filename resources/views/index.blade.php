<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>CityHost FileManager</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <link href="/css/custom.css" rel="stylesheet" type="text/css">
    <link href="/css/toastr.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.30.3/js/jquery.tablesorter.combined.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.2/js/toastr.min.js"></script>
    <script src="//requirejs.org/docs/release/2.3.5/r.js" type="text/javascript" charset="utf-8"></script>
    <script src="/ace/ace.js" type="text/javascript" charset="utf-8"></script>
    <script src="/ace/theme-eclipse.js" type="text/javascript" charset="utf-8"></script>
    <script src="/ace/ext-language_tools.js"></script>
    <script src="/js/app.js?4"></script>
    <script src="/js/FileSaver.js"></script>
</head>
<body>
<div class="modal" id="newdir_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Создание новой директории</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="dirname"><b>Задайте имя для новой директории:</b></label>
                    <input autocomplete="off" class="form-control" type="text" name="dirname" id="dirname">
                </div>
            </div>
            <div class="loader modal-loader" style="display:none;"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="makeDir">Создать</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Отменить</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="newfile_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Создание нового файла</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="filename"><b>Задайте имя для нового файла:</b></label>
                    <input autocomplete="off" class="form-control" type="text" name="filename" id="filename">
                </div>
            </div>
            <div class="loader modal-loader" style="display:none;"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="makeFile">Создать</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Отменить</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="upload_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Загрузка файла на сервер</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="uploadFileInput"><b>Выберите файл <small><small>(макс. размер @php echo ini_get("upload_max_filesize") @endphp)</small></small>:</b></label>
                    <input autocomplete="off" type="file" class="form-control-file" id="uploadFileInput">
                </div>
                <div class="progress progress-upload" style="display: none;">
                    <div class="progress-bar progress-bar-striped progress-bar-upload" role="progressbar" style="width: 0%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">0%</div>
                </div>
            </div>
            <div class="loader modal-loader" style="display:none;"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="uploadFile">Загрузить</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Отменить</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="options_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Дополнительные настройки</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="options_body">
                <div class="form-row form-inline">
                    <div class="form-group col-md-3">
                        <label for="file_name" class="" style="vertical-align: middle;">Имя файла</label>
                    </div>
                    <div class="form-group col-md-9">
                        <input required="" name="file_name_new" id="file_name_new" class="form-control" autocomplete="off" type="text">
                    </div>
                </div>
                <br/>
                <div class="row">
                    <table class="table">
                        <thead>
                        <tr>
                            <td colspan="4" class="text-center font-weight-bold">Права доступа</td>
                        </tr>
                        <tr>
                            <td id="recursive_select" colspan="4" style="display: none;">
                                <select class="form-control form-control-sm" name="change-perm-type" id="change-perm-type">
                                    <option name="only-this" selected="">Без рекурсии</option>
                                    <option name="recursive-file">Сменить права только для файлов</option>
                                    <option name="recursive-dir">Сменить права только для директорий</option>
                                    <option name="recursive-all">Сменить права для файлов и директорий</option>
                                </select>
                            </td>
                        </tr>
                            <tr class="checkbox-table-centered">
                                <th></th>
                                <th>Чтение</th>
                                <th>Запись</th>
                                <th>Испольнение</th>
                            </tr>
                        </thead>
                        <tbody id="file-permission">
                            <tr class="checkbox-table-centered">
                                <td>Владелец</td>
                                <td><input id="owner-read-perm" name="owner-read-perm" data-name="owner" value="4" class="checkbox" type="checkbox"></td>
                                <td><input id="owner-write-perm" name="owner-write-perm" data-name="owner" value="2" class="checkbox" type="checkbox"></td>
                                <td><input id="owner-execution-perm" name="owner-execution-perm" data-name="owner" value="1" class="checkbox" type="checkbox"></td>
                            </tr>
                            <tr class="checkbox-table-centered">
                                <td>Группа</td>
                                <td><input id="group-read-perm" name="group-read-perm" data-name="group" value="4" class="checkbox" type="checkbox"></td>
                                <td><input id="group-write-perm" name="group-write-perm" data-name="group" value="2" class="checkbox" type="checkbox"></td>
                                <td><input id="group-execution-perm" name="group-execution-perm" data-name="group" value="1" class="checkbox" type="checkbox"></td>
                            </tr>
                            <tr class="checkbox-table-centered">
                                <td>Остальные</td>
                                <td><input id="other-read-perm" name="other-read-perm" data-name="other" value="4" class="checkbox" type="checkbox"></td>
                                <td><input id="other-write-perm" name="other-write-perm" data-name="other" value="2" class="checkbox" type="checkbox"></td>
                                <td><input id="other-execution-perm" name="other-execution-perm" data-name="other" value="1" class="checkbox" type="checkbox"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="loader modal-loader" style="display:none;"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="saveOptions">Сохранить</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Отменить</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_fileeditor" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-full" role="document">
        <div class="modal-content">
            <div class="modal-header" id="fileEditorHeader">
                <div class="modal-title form-inline modal-full">
                    <h5 id="fileEditorTitle" class="col-sm-6"><small>Редактирование файла</small></h5>
                    <form class="col-sm-4 form-inline"> <label for="fileEncoding" class="col-sm-3">Кодировка</label>
                        <select id="customFileEncoding" name="customFileEncoding" class="form-control form-control-sm mb-2 mr-sm-2 mb-sm-0">
                            <option value="UTF-8">UTF-8</option>
                            <option value="Windows-1251">Windows-1251</option>
                            <option value="Windows-1252">Windows-1252</option>
                            <option value="KOI8-R">KOI8-R</option>
                            <option value="KOI8-U">KOI8-U</option>
                            <option value="ASCII">ASCII</option>
                            <option value="CP866">CP866</option>
                            <option value="CP936">CP936</option>
                            <option value="CP932">CP932</option>
                            <option value="CP51932">CP51932</option>
                            <option value="CP50220">CP50220</option>
                            <option value="CP50220raw">CP50220raw</option>
                            <option value="CP50221">CP50221</option>
                            <option value="CP50222">CP50222</option>
                            <option value="UTF-32">UTF-32</option>
                            <option value="UTF-32BE">UTF-32BE</option>
                            <option value="UTF-32LE">UTF-32LE</option>
                            <option value="UTF-16">UTF-16</option>
                            <option value="UTF-16BE">UTF-16BE</option>
                            <option value="UTF-16LE">UTF-16LE</option>
                            <option value="UTF-7">UTF-7</option>
                            <option value="UTF7-IMAP">UTF7-IMAP</option>
                            <option value="ISO-2022-KR">ISO-2022-KR</option>
                            <option value="ISO-8859-1">ISO-8859-1</option>
                            <option value="ISO-8859-2">ISO-8859-2</option>
                            <option value="ISO-8859-3">ISO-8859-3</option>
                            <option value="ISO-8859-4">ISO-8859-4</option>
                            <option value="ISO-8859-5">ISO-8859-5</option>
                            <option value="ISO-8859-6">ISO-8859-6</option>
                            <option value="ISO-8859-7">ISO-8859-7</option>
                            <option value="ISO-8859-8">ISO-8859-8</option>
                            <option value="ISO-8859-9">ISO-8859-9</option>
                            <option value="ISO-8859-10">ISO-8859-10</option>
                            <option value="ISO-8859-13">ISO-8859-13</option>
                            <option value="ISO-8859-14">ISO-8859-14</option>
                            <option value="ISO-8859-15">ISO-8859-15</option>
                            <option value="ISO-8859-16">ISO-8859-16</option>
                            <option value="7bit">7bit</option>
                            <option value="8bit">8bit</option>>
                        </select>
                    </form>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>

            </div>
            <div class="modal-body" id="fileEditor"></div>
            <div class="loader modal-loader" id="fileEditorLoader" style="display:none;"></div>
            <div class="modal   " id="fileEditorLoader" style="display:none;"></div>
            <div class="modal-footer" id="fileEditorFooter">
                <button type="button" class="btn btn-sm btn-secondary btn-close-fileedit" data-dismiss="modal">Закрыть</button>
                <button type="button" class="btn btn-sm btn-primary" id="fileEditorSave" data-filename="">Сохранить</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_confirm" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-body">
                <p id="confirm-text"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="btn-confirm-yes">Подтверждаю</button>
                <button type="button" class="btn btn-primary" id="btn-confirm-no">Отменить</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_download_dir" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-body">
                <p>Скачать директорию, используя функционал быстрого скачивания файлов - невозможно.</p>
                <p>Для скачивания директории Вы можете :</p>
                <ol>
                    <li>либо создать архив (<button class="btn btn-primary btn-sm" style="cursor: default; height: 20px; width: 20px;  padding: 0px;  margin: 0px;"><i class="fas fa-archive"></i></button>) нужной директории и скачать его;</li>
                    <li>либо, если вам необходимо скачать копию сайта, Вы можете сделать это при помощи создания резервной копии текущего состояния сайта из панели управления :<br/>
                    <a href="https://cityhost.ua/support/hosting/rabota-s-rezervnym-kopirovaniem/kak-sozdat-rezervnuyu-kopiyu-tekuschego-sostoyaniya-sayta/" target="_blank">инструкция</a></li>
                </ol>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="check_inodes_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Информация о занятом пространстве</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Количество инодов в директории: <span id="count_innodes_in_dir"></span></p>
                <p>Размер директории: <span id="directory_size"></span></p>
            </div>
            <div class="loader modal-loader" style="display:none;"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="check_inodes">Обновить данные</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<div id="main">
    <section class="content">
        <div class="row col-12">
            <div class="col-1">
                <a href="https://cityhost.ua"><img src="https://cityhost.ua/assets/images/logo.svg" width="148px" height="66px"></a>
            </div>
            <div class="offset-7 col-4 well-sm" id="tools">
                <button class="btn btn-sm btn-primary" id="upload-button"  data-toggle="modal" data-target="#upload_modal" data-backdrop="static" data-keyboard="false" data-tooltipe="yes" data-placement="bottom" title="Загрузить файл"><i class="fas fa-upload"></i> </button>
                <button class="btn btn-sm btn-primary" id="download-button" disabled="" onclick="download()" data-tooltipe="yes" data-placement="bottom" title="Скачать выбранные файлы"><i class="fas fa-download"></i> </button>
                <button class="btn btn-sm btn-primary" id="new-file-button" data-toggle="modal" data-target="#newfile_modal"  data-backdrop="static" data-keyboard="false" data-tooltipe="yes" data-placement="bottom" title="Создать файл"><i class="fas fa-file"></i> </button>
                <button class="btn btn-sm btn-primary" id="new-folder-button" data-toggle="modal" data-target="#newdir_modal"  data-backdrop="static" data-keyboard="false" data-tooltipe="yes" data-placement="bottom" title="Создать директорию"><i class="fas fa-folder-open"></i> </button>
                <button class="btn btn-sm btn-primary" id="extract-zip" disabled="" data-tooltipe="yes" data-placement="bottom" title="Извлечь архив"><i class="far fa-file-archive"></i> </button>
                <button class="btn btn-sm btn-primary" id="make-archive" disabled="" data-tooltipe="yes" data-placement="bottom" title="Создать архив"><i class="fas fa-archive"></i> </button>
                <button class="btn btn-sm btn-primary" id="copy-button" disabled="" data-tooltipe="yes" data-placement="bottom" title="Копировать"><i class="fas fa-copy"></i></button>
                <button class="btn btn-sm btn-primary" id="cut-button" disabled="" data-tooltipe="yes" data-placement="bottom" title="Вырезать"><i class="fas fa-cut"></i></button>
                <button class="btn btn-sm btn-primary" id="paste-button" disabled="" data-tooltipe="yes" data-placement="bottom" title="Вставить"><i class="fas fa-paste"></i></button>
                <button class="btn btn-sm btn-primary" id="refresh-button" onclick="updateFileList()" data-tooltipe="yes" data-placement="bottom" title="Обновить список файлов"><i class="fas fa-sync-alt"></i> </button>
                <button class="btn btn-sm btn-danger" id="remove-button" disabled="" data-tooltipe="yes" data-placement="bottom" title="Удалить" autocomplete=""><i class="fas fa-trash-alt"></i> </button>
                <button class="btn btn-sm btn-danger" id="logout-button" data-tooltipe="yes" data-placement="bottom" title="Выйти"><i class="fas fa-sign-out-alt"></i></button>
            </div>
        </div>
        <div class="row col-sm-12">
        <table class="table table-hover table-condensed fileListContent" id="fileListContent" style="display: none;">
            <thead>
            <tr>
                <th scope="col"><input type='checkbox' id='selectAll' class='checkbox' title="Выбрать все"></th>
                <th scope="col">Имя</th>
                <th scope="col"></th>
                <th scope="col"></th>
                <th scope="col">Права</th>
                <th scope="col">Последнее изменение</th>
                <th scope="col" class="sorter-metric" data-metric-name-full="byte|Byte|BYTE" data-metric-name-abbr="b|B">Размер</th>
            </tr>
            </thead>
            <tr role='row' id="currdir"><td colspan='6'><span id="dir"></span></td></tr>
            <tbody id="filelist">
            </tbody>
        </table>
        </div>
        <div class="loader modal-loader" id="fileListLoader" style="display:none;"></div>
    </section>
</div>
</body>
</html>
