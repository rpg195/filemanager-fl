<?php
/**
 * Created by PhpStorm.
 * User: andriy
 * Date: 03.05.18
 * Time: 13:24
 */

namespace App\Http\Controllers;


use App\Http\Libs\FtpWrapper;
use App\Http\Libs\ApiHostings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;


class Fm
{
    protected $ftp;
    protected $apiHostings;
    private $ftpserver;
    private $ftppassword;
    private $ftpusername;
    private $ftpport;
    private $currentDir;
    private $editable;
    public function __construct(FtpWrapper $ftp, ApiHostings $apiHostings)
    {
        $this->apiHostings = $apiHostings;
        $this->ftp = $ftp;
        $this->ftpusername = session("ftpusername");
        $this->ftppassword = session("ftppassword");
        $this->ftpport = session("ftpport");
        $this->ftpserver =  session("ftpserver");
        $this->currentDir = (session('currentdir'){0} == '/') ? session('currentdir') : "/".session('currentdir');
        $this->editable = config("app.editable_file_ext");
    }

    function index()
    {
        if (!$this->ftpusername && !$this->ftppassword && !$this->ftpport && !$this->ftpserver) {
            return redirect("/");
        }
        return view("index");
    }

    function ConvertChmodToInt($chmod) {
        $owner_mod = 0;
        $owner_mod += ($chmod{1} == 'r') ? 4 : 0;
        $owner_mod += ($chmod{2} == 'w') ? 2 : 0;
        $owner_mod += ($chmod{3} == 'x') ? 1 : 0;
        $group_mod = 0;
        $group_mod += ($chmod{4} == 'r') ? 4 : 0;
        $group_mod += ($chmod{5} == 'w') ? 2 : 0;
        $group_mod += ($chmod{6} == 'x') ? 1 : 0;
        $other_mod = 0;
        $other_mod += ($chmod{7} == 'r') ? 4 : 0;
        $other_mod += ($chmod{8} == 'w') ? 2 : 0;
        $other_mod += ($chmod{9} == 'x') ? 1 : 0;
        return 0 . $owner_mod . $group_mod . $other_mod;
    }

    function GetFiles(Request $request)
    {
        if (!empty($request->get('dir'))) {
            $dir = Session::get("currentdir") . "{$request->get('dir')}/";
        } elseif (!empty(Session::get("currentdir"))) {
            $dir = session("currentdir");
        } else {
            $dir = "/";
        }
        Session::put(["currentdir" => $dir]);
        Session::save();
        $filedata = array();
        $list = $this->ftp->getList($dir);
        foreach ($list['folders'] as $val) {
            $filedata[$val['name']]['name'] = $val['name'];
            $filedata[$val['name']]['size'] = $this->formatBytes($val['size']);
            $filedata[$val['name']]['type'] = 'dir';
            $filedata[$val['name']]['modify_c'] = "";
            $filedata[$val['name']]['chmod_int'] = $this->ConvertChmodToInt($val['chmod']);
        }
        foreach ($list['files'] as $val) {
            $val['name'] = mb_convert_encoding($val['name'],"UTF-8");
            $filedata[$val['name']]['name'] = $val['name'];
            $size = $this->ftp->getFileSize($val['name']);
            $filedata[$val['name']]['size'] = $this->formatBytes($size);
            $filedata[$val['name']]['type'] = 'file';
            $filedata[$val['name']]['modify_c'] = date("d-m-Y H:i:s", $this->ftp->getModifyTime($dir.$val['name']));
            $filedata[$val['name']]['chmod_int'] = $this->ConvertChmodToInt($val['chmod']);
            if($size < 0) {
                unset($filedata[$val['name']]);
            }
        }
        array_unshift($filedata, ['currentdir' => $dir]);

//        dd($filedata);

        $return = json_encode($filedata);

        return response($return,200);
    }

    function UpDir(Request $request)
    {
        if (empty($request->get('action')) || $request->get('action') != 'updir') {
            return 'err';
        } else {
            $olddir = $this->currentDir;
            $arr = explode("/", $olddir);
            $arr = array_filter($arr);
            array_pop($arr);
            if (count($arr) == 0) {
                $newdir = "/";
            } else {
                $newdir = implode("/", $arr);
                $newdir = "/".$newdir . "/";
            }
            session(['currentdir' => $newdir]);
            return json_encode(array("status" => "ok"));
        }
    }

    function GetFileContent(Request $request)
    {
        if (empty($request->get('filename'))) {
            return 'err';
        } else {
            $filepath = "{$this->currentDir}/{$request->get('filename')}";
            $ext = pathinfo($filepath, PATHINFO_EXTENSION);
            if(!in_array($ext,$this->editable)) {
                return json_encode(['status' => "unsupported_file_extension", 'content' => '']);
            }
            session(['FileEditName' => $request->get('filename')]);
            $content = $this->ftp->getFileContent(session('FileEditName'));
            $custom_encoding = (!empty($request->get('customFileEncoding'))) ? $request->get("customFileEncoding") : "UTF-8";

            session(['FileEditEncoding' => $custom_encoding]);
            $result = $this->convertContent($content, $custom_encoding, "UTF-8");

            return json_encode(['status'=>'success', 'content' => $result]);
        }
    }

    function SaveFile(Request $request)
    {
        $filename = $request->get('filename');
        $pathToSave = $this->currentDir . $filename;
        $content = $request->get('content');
        $fileencoding = $request->get('encoding');
        if($fileencoding == 'enc_not_supp') {
            return 'enc_not_supp';
        }
        if (!empty($filename) && session('FileEditName') == $request->get('filename')) {
            $toSave = urldecode($content);
            $toSave = $this->convertContent($toSave,"UTF-8",$fileencoding);
            return $this->ftp->saveFile($pathToSave, $toSave);
        } else {
            return "Filename cannot be empty!";
        }
    }

    function formatBytes($size)
    {
        $units = array('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
        $power = $size > 0 ? floor(log($size, 1024)) : 0;
        $response = number_format($size / pow(1024, $power), 2, '.', ',') . ' ' . $units[$power];
        return $response;
    }

    function convertContent($content, $from, $to)
    {
        $encodings = [
            "UTF-8",
            "Windows-1251",
            "Windows-1252",
            "KOI8-R",
            "KOI8-U",
            "ASCII",
            "ArmSCII-8",
            "CP866",
            "CP936",
            "CP932",
            "CP51932",
            "CP50220",
            "CP50220raw",
            "CP50221",
            "CP50222",
            "UTF-32",
            "UTF-32BE",
            "UTF-32LE",
            "UTF-16",
            "UTF-16BE",
            "UTF-16LE",
            "UTF-7",
            "UTF7-IMAP",
            "ISO-2022-KR",
            "ISO-8859-1",
            "ISO-8859-2",
            "ISO-8859-3",
            "ISO-8859-4",
            "ISO-8859-5",
            "ISO-8859-6",
            "ISO-8859-7",
            "ISO-8859-8",
            "ISO-8859-9",
            "ISO-8859-10",
            "ISO-8859-13",
            "ISO-8859-14",
            "ISO-8859-15",
            "ISO-8859-16",
            "7bit",
            "8bit",
        ];
        if (!in_array($to, $encodings)) {
            return "enc_not_supp";
        }
        return mb_convert_encoding($content, $to, $from);
    }

    function makeDir(Request $request)
    {
        if (!empty($request->get('dirname'))) {
            $dirname = $request->get('dirname');
            return $this->ftp->makeDir($dirname);
        } else {
            return "dir name cannot be empty";
        }
    }

    function makeFile(Request $request)
    {
        if (!empty($request->get('filename'))) {
            $filename = $request->get('filename');
            return $this->ftp->makeFile($filename);
        } else {
            return "file name cannot be empty";
        }
    }

    function download(Request $request) {
        $files = $request->get('files');
        if(count($files) > 1 || count($files) < 1) {
            return response("only_one_file_can_be_download");
        }

        $file_to_download = $files[0];


        $local_download_path = config("app.download_path").$this->ftpusername;

        if(!file_exists($local_download_path)) {
            mkdir($local_download_path,0755,true);
        }
        $local_file_path = $local_download_path . '/' . $file_to_download;

        $remote_path = $this->currentDir . '/' . $file_to_download;

        $current_dir_list = $this->ftp->getList($this->currentDir);

        foreach ($current_dir_list['folders'] as $folder) {
            if($folder['name'] == $file_to_download) {
                return response("only_files_can_be_download");
            }
        }


        $this->ftp->download($remote_path,$local_file_path);

        return response('/downloads/'.$this->ftpusername. "/" .$file_to_download);
    }

    function uploadFile(Request $request) {
        if($request->file('file')->getError() !== 0) {
            return $request->file('file')->getErrorMessage();
        }

        $file = $request->file('file');

        if(preg_match($this->ftp->restricted_chars,$file->getClientOriginalName())) {
            return "err_has_restricted_chars";
        }

        $file_arr['filename'] = $file->getClientOriginalName();


        $file_arr['extension'] = $file->extension();
        $file_arr['size'] = $file->getSize();
        $file_arr['mime'] = $file->getMimeType();
        $file_arr['path'] = $file->getPathname();
        $tmp_dir = config("app.uploads_tmp");
        $file_arr['localpath'] =$tmp_dir.$file_arr['filename'];
        $file_arr['uploadpath'] =$this->currentDir.$file_arr['filename'];
        copy($file_arr['path'], $file_arr['localpath']);
        $file_arr['path'] = $file->getPathname();
        if($file_arr['size'] > 309715200) {
            return "err file too large";
        }
        if($this->ftp->uploadFile($file_arr,$file)){
            unlink($file_arr['path2']);
            unlink($tmp_dir);
        }
    }

    function remove(Request $request) {
        if(empty($request->get("files"))) {
            return "err_file_list_not_passed";
        }
        $files = $request->get("files");

        foreach ($files as $file) {
            $remove = $this->ftp->remove($file, $this->currentDir);
            if($remove) {
                $removable[$file] = 1;
            } else {
                $removable[$file] = 0;
            }
        }

        if(in_array(0,$removable)) {
            return json_encode($removable);
        } else {
            return "success";
        }

    }

    function changeFileOptions(Request $request) {
        $data = [
            'filename' => $request->get("filename"),
            'filename_new' => $request->get("filename_new"),
            'chmod' => $request->get("chmod"),
            'recursive' => $request->get("recursive"),
        ];
        $chfilename = 'ok';
        $chmod = $this->ftp->chmod($data['filename'],$data['chmod'],$data['recursive']);
        if($data['filename'] != $data['filename_new']) {
            $chfilename = $this->ftp->rename($data['filename'],$data['filename_new']);
        }

        return response(json_encode(['filename'=>$chfilename, 'chmod'=>$chmod]),200);
    }

    function unzip(Request $request) {
        if(empty($request->get('filename'))) {
            return response('filename_not_passed');
        }

        $zip_name = $request->get('filename');
        $remotedir = $this->currentDir;
        $return = $this->apiHostings->unzip($this->ftpserver,$remotedir,$zip_name,$this->ftpusername);
        if($return['msg'] == 'success') {
            $response = 'ok';
        } else {
            $response = $return['msg'];
        }
        return response($response,200);
    }

    function makeArchive(Request $request) {
        if(empty($request->get('filelist'))) {
            return response('filelist_not_passed');
        }

        $remotedir = $this->currentDir;
        $return = $this->apiHostings->makeArchive($this->ftpserver,$remotedir,$request->get('filelist'),$this->ftpusername);
        if($return['msg'] == 'success') {
            $response = 'ok';
        } else {
            $response = $return['msg'];
        }
        return response($response,200);
    }


    function move(Request $request){
        if (empty($request->get('files') && $request->get('action') && $request->get('todir'))) {
            return response("not_passed_all_data", 200);
        }
        $files = $request->get('files');
        $action = $request->get('action');
        $todir = $request->get('todir');
        foreach ($files as $k) {
            $try = $this->ftp->move($k, $todir, $action);
            if($try != 'ok') {
                $return[] = $k;
            }
        }
        if(isset($return) && !empty($return)) {
            return response(json_encode(['status'=>'err', 'files' => $return]),200);
        }
        return response(json_encode(['status'=>"ok"]),200);
    }

    function checkInodes(Request $request) {
        if(empty($request->get("dirname"))) {
            return response(json_encode(['status' => 'err', 'message' => 'dirname not defined']));
        }
        $dirname = $request->get('dirname');

        $res = $this->apiHostings->checkInodes($dirname,$this->ftpusername,$this->ftpserver,$this->currentDir);
        $session_name = "checkInodes_{$this->currentDir}/$dirname";
        if(isset($res['msg']['inodes'])) {
            Session::put($session_name,$res['msg']);
            return response(json_encode(['status'=>'success','message' => $res['msg']]));
        } else {
            return response(json_encode(['status'=>'success','message' => $res]));
        }
    }

    function getLastInodesValue(Request $request) {
        if(empty($request->get("dirname"))) {
            return response(json_encode(['status' => 'err', 'message' => 'dirname not defined']));
        }
        $dirname = $request->get('dirname');
        $session_name = "checkInodes_{$this->currentDir}/$dirname";
        $value = Session::get($session_name);
        if(!$value) {
            return response(json_encode(['status' => 'err', 'message' => 'no_data']));
        } else {
            return response(json_encode(['status' => 'success', 'message' => $value]));
        }
    }


}

