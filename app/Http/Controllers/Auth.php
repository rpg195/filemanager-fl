<?php

namespace App\Http\Controllers;
use App\Http\Libs\FtpWrapper;
use Illuminate\Http\Request;


class Auth extends Controller
{
    protected $ftp;
    public function __construct(FtpWrapper $ftp)
    {
        $this->ftp = $ftp;
    }

    public function index(Request $request){
        if($request->has("FtpUsername") && $request->filled("FtpUsername")) {
            $data['FtpUsername']['data'] = $request->get('FtpUsername');
        } else {
	    $data['FtpUsername']['data'] = "";
            $data['FtpUsername']['err'] = 'empty';
            $data['err'] = 'empty';
        }

        if($request->has("FtpPassword") && $request->filled("FtpPassword")) {
            $data['FtpPassword']['data'] = $request->get('FtpPassword');
        } else {
  	    $data['FtpPassword']['data'] = "";
            $data['FtpPassword']['err'] = 'empty';
            $data['err'] = 'empty';
        }

        if($request->has("ServerIp") && $request->filled("ServerIp")) {
            $data['ServerIp']['data'] = $request->get('ServerIp');
        } else {
	    $data['ServerIp']['data'] = "";
            $data['ServerIp']['err'] = 'empty';
            $data['err'] = 'empty';
        }
	session()->remove("currentdir");
        session()->remove("ftpserver");
        session()->remove("ftpusername");
        session()->remove("ftppassword");
        session([
            'ftpserver' => $data['ServerIp']['data'],
            'ftpusername' => $data['FtpUsername']['data'],
            'ftppassword' => $data['FtpPassword']['data']
        ]);
        if(in_array('empty',$data)) {
            return view("login",$data);
        } else {
            $response = $this->ftp->authOnFtpServer($data['ServerIp']['data'],$data['FtpUsername']['data'],$data['FtpPassword']['data']);
            if(!isset($response['err'])) {
                return redirect("/index",301);
            }  else {
                return view("login",['errmsg'=>$response]);
            }
        }

    }
}
