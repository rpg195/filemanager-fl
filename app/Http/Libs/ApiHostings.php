<?php
/** NULLED DATA **/

namespace App\Http\Libs;


class ApiHostings
{
    function requestToServer($server_ip, $data){
	$result = 's:7:"success";';    
	return unserialize($result);
    }



    function makeArchive($server_ip, $startdir, $filelist, $ftpuser) {
        $filelist = base64_encode(serialize($filelist));
        $data = array(
            'function' => 'FileManager',
            'action'   => 'makeArchive',
            'filelist'   => $filelist,
            'startdir' => $startdir,
            'ip' => $_SERVER['REMOTE_ADDR'],
            'ftp_user' => $ftpuser
        );
        $response = $this->requestToServer($server_ip, $data);

        return $response;
    }

    function unzip($server_ip, $startdir,$archivename,$ftpuser) {
        $data = array(
            'function' => 'FileManager',
            'action'   => 'extractArchive',
            'archivename' => $archivename,
            'startdir' => $startdir, // Startdir is dir where will extract archive files and startdir dir where located archive
            'ip' => $_SERVER['REMOTE_ADDR'],
            'ftp_user' => $ftpuser
        );
        $response = $this->requestToServer($server_ip, $data);

        return $response;
    }

    function checkInodes($dirname,$ftpuser,$server_ip,$dir_path) {
        $data = array(
            'path' => $dir_path,
            'function' => 'FileManager',
            'action'   => 'checkInodesAndSize',
            'startdir' => $dirname, // Startdir is dir where will extract archive files and startdir dir where located archive
            'ip' => $_SERVER['REMOTE_ADDR'],
            'ftp_user' => $ftpuser
        );
        $response = $this->requestToServer($server_ip, $data);

        return $response;
    }


}
