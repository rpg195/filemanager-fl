<?php

namespace App\Http\Libs;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Mockery\Exception;
use Symfony\Component\Finder\Iterator\RecursiveDirectoryIterator;
use wapmorgan\UnifiedArchive\UnifiedArchive;

class FtpWrapper
{
    private $hostname;
    private $username;
    private $password;
    private $port;
    private $currentDir;
    public $restricted_chars;
    public $user_upload_tmp;
    /**
     * @var string ftp_connect()
     */
    private $connect;
    protected $ftp;
    public function __construct()
    {

        if(App::environment() == 'development') {
            $this->connect = "ftp_connect";
            $this->ftpProtocol = "ftp://";
        } else {
            $this->connect = 'ftp_ssl_connect';
            $this->ftpProtocol = "ftps://";
        }

        $hostname = (filter_var(session('ftpserver'),FILTER_VALIDATE_IP)) ? session('ftpserver') : null;
        $port = (filter_var(session("ftpport"),FILTER_VALIDATE_INT)) ? session("ftpport") : 21;
        $this->hostname = $hostname;
        $this->username = session('ftpusername');
        $this->password = session('ftppassword');
        $this->port = $port;
        $this->ftp = $this->authOnFtpServer();
        $this->currentDir = session('currentdir');
        $this->restricted_chars = "~(\\|\/|:|\*|\?|\"|<|>|\||\+|\~| )~";
        $this->user_upload_tmp  = config("app.uploads_tmp").$this->username."/";
        @mkdir($this->user_upload_tmp);



    }

    function authOnFtpServer($host = null, $user = null, $pass = null, $port = null) {
        if(!empty($host && $user && $pass)) {
            $this->hostname = $host;
            $this->port = $port;
            $this->username = $user;
            $this->password = $pass;
        }
        try {

            $ftp = call_user_func($this->connect,$this->hostname,$this->port);
//            $ftp = $this->$connect($this->hostname,$this->port);
            ftp_login($ftp,$this->username,$this->password);

            ftp_pasv($ftp,FALSE);
            return $ftp;
        } catch (\Exception $e) {
            $returnmsg['err'] = true;
            $returnmsg['errmsg'] = 'Не верные данные авторизации';
            return $returnmsg;
        }
        return true;
    }

    function ftpClose($ftp) {
        return ftp_close($ftp);
    }
    function getFileSize($filename) {
        return ftp_size($this->ftp,session('currentdir').$filename);
    }
    function chDir($dir) {
        return ftp_chdir($this->ftp,$dir);
    }

    function currentDir() {
        return ftp_pwd($this->ftp);
    }

    function getFileContent($filename) {
        ob_start();
        ftp_get($this->ftp, "php://output", "{$this->currentDir}/{$filename}", FTP_BINARY);
        $data = ob_get_contents();
        ob_end_clean();
        return $data;
    }

    function saveFile($pathToSave,$content) {
        $data = fopen("data://text/plain," . urlencode($content),'rb');
        try {
            ftp_fput($this->ftp,$pathToSave,$data, FTP_BINARY);
            return 'ok';
        } catch (\Exception $e) {
            return stristr($e->getMessage()," /");
        }
    }
    function makeDir($dirname) {
        if(preg_match($this->restricted_chars,$dirname)) {
            return "err_has_restricted_chars";
        } else {
            $dir_to_create = $this->currentDir.$dirname;
            if(!$this->checkIsExitst($dir_to_create)) {
                return ftp_mkdir($this->ftp, $dir_to_create);
            }else {
                return "dir_already_exists";
            }
        }
    }
    function makeFile($filename) {
        if(preg_match($this->restricted_chars,$filename)) {
            return "err_has_restricted_chars";
        } else {
            $file_to_create = $this->currentDir.$filename;
            if(!$this->checkIsExitst($file_to_create)) {
                if($this->saveFile($file_to_create,"")) {
                    return "ok";
                } else {
                    return "err";
                }
            } else {
                return "file_already_exists";
            }
        }
        $fp = fopen('php://temp', 'r+');
        fwrite($fp, $out);
        rewind($fp);
        ftp_fput($ftp_conn, $remote_file_name, $fp, FTP_ASCII);
    }

    function checkIsExitst($path) {
        $list = ftp_nlist($this->ftp,$path);
        return $list;
    }

    function uploadFile($data) {
        $remote_path = $data['uploadpath'];
        $local_path = $data['localpath'];
        $ftp = $this->ftp;
        $size = $data['size'];
	$hostname = gethostbyaddr($this->hostname);
	$context = ['ssl'=> 
			[
				"verify_peer_name"=>false,
				"verify_peer" => false,
			],
		   ];
	$password = urlencode($this->password);
        $url = $this->ftpProtocol . "{$this->username}:{$password}@{$hostname}/{$remote_path}";
        $size = filesize($local_path) or die("Cannot retrieve size file");
        $hout = fopen($url, "wb",false,stream_context_create($context)) or die("Cannot open destination file");
        $hin = fopen($local_path, "rb") or die("Cannot open source file");
        ob_start();
        while (!feof($hin))
        {
            $buf = fread($hin, 10240);
            fwrite($hout, $buf);
            echo intval(ftell($hin)/$size*50).",";
            flush();
            ob_flush();
        }

        echo "\n";

        fclose($hin);
        fclose($hout);
        unlink($local_path);
    }


    function download($remotepath,$localpath) {
        ftp_get($this->ftp,$localpath,$remotepath,FTP_BINARY);
        return $localpath;
    }

    function getType($name, $type = 'files', $fullpath = null) {

        $all = $this->getList($fullpath);
        foreach ($all[$type] as $k=>$v) {
            if($v['name'] == $name) {
                return $type;
            }
        }
        return (isset($return)) ? $return : 'not_found';
    }

    function remove($name, $fullpath) {
        $type = $this->getType($name, 'files', $fullpath);
        if($type == 'not_found') {
            $type2 = $this->getType($name, 'folders', $fullpath);
            if ($type2 == 'not_found') {
                return "err";
            } else {
                $type = 'dir';
            }
        }
        if($type == 'dir') {
            return $this->removeRecursive($fullpath.$name);
        } else {
            return ftp_delete($this->ftp,$fullpath.$name);
        }
    }

    function getList($path, $recursive = false) {
        $i             = 0;
        $files         = array();
        $folders       = array();
        $statusnext    = false;
        $currentfolder = $path;

        $list = ftp_rawlist($this->ftp, $currentfolder, $recursive);
        foreach ($list as $current) {
            if (empty($current)) {
                $statusnext = true;
                continue;
            }
            if ($statusnext === true) {
                $currentfolder = substr($current, 0, -1);
                $statusnext = false;
                continue;
            }
            $split = preg_split('[ ]', $current, 9, PREG_SPLIT_NO_EMPTY);
            $entry = $split[8];
            $isdir = ($split[0]{0} === 'd') ? true : false;
            if(!$recursive) {
                $chmod = $split[0];
                $size = $split[4];
                $mtime = "$split[6] $split[5] $split[7]";
            }
            if ($entry === '.' || $entry === '..') {
                continue;
            }

            if($recursive) {
                if ($isdir === true) {
                    $folders[] = $currentfolder . '/' . $entry;
                } else {
                    $files[] = $currentfolder . '/' . $entry;
                }
            } else {
                if ($isdir === true) {
                    $folders[$entry]['name'] = $entry;
                    $folders[$entry]['chmod'] = $chmod;
                    $folders[$entry]['size'] = $size;
                    $folders[$entry]['mtime'] = $mtime;
                } else {
                    $files[$entry]['name'] = $entry;
                    $files[$entry]['chmod'] = $chmod;
                    $files[$entry]['size'] = $size;
                    $files[$entry]['mtime'] = $mtime;
                }
            }
        }
        $return = ['files' => $files, 'folders' => $folders];
        return $return;
    }
    function getModifyTime($path) {
        return ftp_mdtm($this->ftp,$path);
    }
    function removeRecursive($path) {
        $list = $this->getList($path,true);
        foreach ($list['files'] as $file) {
            ftp_delete($this->ftp, $file);
        }

        // Delete all the directories
        // Reverse sort the folders so the deepest directories are unset first
        rsort($list['folders']);
        foreach ($list['folders'] as $folder) {
            ftp_rmdir($this->ftp, $folder);
        }

        // Delete the final folder and return its status
        return ftp_rmdir($this->ftp, $path);
    }

    function chmod($filename, $chmod, $recursive = 'none'){
        $filepath = $this->currentDir.$filename;
        if($recursive != 'none') {
            $list = $this->getList($filepath,true);
        }


        $chmod_n = octdec( 0 . $chmod);

        ftp_chmod($this->ftp,$chmod_n,$filepath);

        switch ($recursive) {
            case 'files':
                foreach ($list['files'] as $file) {
                    try {
                        ftp_chmod($this->ftp, $chmod_n, $file);
                    } catch (\Exception $e) {
                        $err[$file] = stristr($e->getMessage()," /");
                    }
                }
                if(isset($err) && !empty($err)) {
                    return $err;
                } else {
                    return 'ok';
                }
                break;
            case 'dirs':
                foreach ($list['folders'] as $folder) {
                    try {
                        ftp_chmod($this->ftp, $chmod_n, $folder);
                    } catch (\Exception $e) {
                        return $err[$folder] = stristr($e->getMessage()," /");
                    }
                }
                if(isset($err) && !empty($err)) {
                    return $err;
                } else {
                    return 'ok';
                }
                break;
            case 'all':
                foreach ($list['files'] as $file) {
                    try {
                        ftp_chmod($this->ftp, $chmod_n, $file);

                    } catch (\Exception $e) {
                         $err[$file] = stristr($e->getMessage()," /");
                    }
                }
                foreach ($list['folders'] as $folder) {
                    try {
                        ftp_chmod($this->ftp, $chmod_n, $folder);
                    } catch (\Exception $e) {
                        $err[$folder] = stristr($e->getMessage()," /");
                    }
                }
                if(isset($err) && !empty($err)) {
                    return $err;
                } else {
                    return 'ok';
                }
                break;
            default:
                try {
                    ftp_chmod($this->ftp, $chmod_n, $filepath);
                    return 'ok';
                } catch (\Exception $e) {
                    return $err[$filepath] = stristr($e->getMessage()," /");
                }
        }


    }

    function rename($oldname,$newname) {
        $old = $this->currentDir.$oldname;
        $new = $this->currentDir.$newname;
        if(preg_match($this->restricted_chars,$new)) {
            return "err_has_restricted_chars";
        }
        try {
            ftp_rename($this->ftp,$old,$new);
            return "ok";
        } catch (\Exception $e) {
            return stristr($e->getMessage()," /");
        }

    }

    function move($oldname,$newname,$action) {
        $old = $oldname;
        $name = explode("/",$old);
        $name = array_reverse($name);
        $name = $name[key($name)];

        $new = $newname.$name;


        if(preg_match($this->restricted_chars,$new)) {
            return "err_has_restricted_chars";
        }
        try {

            if($action == 'cut') {;
                ftp_rename($this->ftp,$old,$new);
//                $dd = $this->remove($name,$old);
            } else {
	        $context = ['ssl'=>
                        [
                                "verify_peer_name"=>false,
                                "verify_peer" => false,
                        ],
                   ];

                copy($this->ftpProtocol . "{$this->username}:{$this->password}@{$this->hostname}/$old",$this->ftpProtocol . "{$this->username}:{$this->password}@{$this->hostname}/$new",stream_context_create($context));
            }
            return "ok";
        } catch (\Exception $e) {
	    dd($e->getMessage());
            return stristr($e->getMessage()," /");
        }

    }

    function downloadAsArchive($filelist) {

        $uuid = strtoupper(substr(sha1(rand(0,999).time().rand(0,999)),0,10));
        $local_tmp_dir = $this->user_upload_tmp."/".$uuid."/";


        foreach($filelist as $file) {
            if($this->getType($file, "files", $this->currentDir) === 'files') {
                $toDownload[] = $file;
            }

            if($this->getType($file, "folders", $this->currentDir) === 'folders') {
                $toDownload[] = $this->getList($this->currentDir.$file, true);
            }

        }
        unset($file);
        if(!isset($toDownload) || empty(array_filter($toDownload)) || !$toDownload) {
            return 'error';
        }



        ///Make ZIP Archive
        $zipName = $uuid.".zip";
        $zipDirPath = config("app.download_path").$this->username;
        $zipPath = $zipDirPath."/".$zipName;

        if(!file_exists($zipDirPath)) {
            mkdir($zipDirPath,0755,true);
        }


        //Old archive directory

//        $zip = new \ZipArchive();
//        if($zip->open($zipPath,\ZipArchive::CREATE) !== TRUE) {
//            return "error_make_archive";
//        }


        mkdir($local_tmp_dir,0755,true);
        foreach ($toDownload as $item) {
            unset($local_path);
            if(is_array($item)) {
                foreach ($item['folders'] as $folder) {
                    $local_path = $local_tmp_dir.$folder;
                    if(!file_exists($local_path)) {
                        mkdir($local_path,0755,true);
                    }
                    $toArchive[$folder] = $local_path;
                }

                foreach ($item['files'] as $file) {
                    $local_path = $local_tmp_dir.$file;
                    $this->download($file ,$local_path);
                    $toArchive[$file] = $local_path;
                }

            } else {
                $local_path = $local_tmp_dir.$item;
                $this->download($this->currentDir."/".$item,$local_path);
                $toArchive[$item] = $local_path;
            }
        }

        unset($item);

        try {
            UnifiedArchive::archiveDirectory($local_tmp_dir,$zipPath);
            $this->rrmdir($local_tmp_dir);
            return $zipName;
        } catch (\Exception $e) {
            $this->rrmdir($local_tmp_dir);
            @unlink($zipDir);
            return $e->getMessage();
        }

//        chdir($local_tmp_dir);
//        foreach ($toArchive as $archivePath => $localPath) {
//            $fName = ltrim($archivePath,$this->currentDir.",/");
//
//            if(is_dir($localPath)) {
//                $zip->addEmptyDir($fName);
//            } else {
//                $zip->addFile($localPath,$fName);
//            }
//        }
//
//        $zip->close();




    }

    private function rrmdir($path) {
        if(is_dir($path)) {
            array_map([$this,'rrmdir'],glob($path.DIRECTORY_SEPARATOR."{,.[!.]}*",GLOB_BRACE));
            @rmdir($path);
        } else {
            @unlink($path);
        }
    }

}
