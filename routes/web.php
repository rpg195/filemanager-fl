<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => ['web']], function () {
#    Route::get('/', function () {
#        return view('login');
#    });
    Route::get('/', function () {
        if(isset($_GET['ip'])) {
            $data['ServerIp']["data"] = $_GET['ip'];
        }else {
            $data['ServerIp']["data"] = "";
        }
        return view('login',$data);
    });
    Route::get('/login', function () {
        return view('login');
    });

    Route::post('/login', "\App\Http\Controllers\Auth@index");

    Route::get('/index', "\App\Http\Controllers\Fm@index");

    Route::post('/GetFiles', "\App\Http\Controllers\Fm@GetFiles");
    Route::post('/GetFileContent',"\App\Http\Controllers\Fm@GetFileContent");
    Route::post('/UpDir', "\App\Http\Controllers\Fm@UpDir");
    Route::post('/SaveFile', "\App\Http\Controllers\Fm@SaveFile");
    Route::post('/makeDir', "\App\Http\Controllers\Fm@makeDir");
    Route::post('/makeFile', "\App\Http\Controllers\Fm@makeFile");
    Route::post('/uploadFile', "\App\Http\Controllers\Fm@uploadFile");
    Route::post('/download', "\App\Http\Controllers\Fm@download");
    Route::post('/remove', "\App\Http\Controllers\Fm@remove");
    Route::post('/changeFileOptions', "\App\Http\Controllers\Fm@changeFileOptions");
    Route::post('/unzip', "\App\Http\Controllers\Fm@unzip");
    Route::post('/makeArchive', "\App\Http\Controllers\Fm@makeArchive");
    Route::post('/move', "\App\Http\Controllers\Fm@move");
    Route::post('/checkInodes', "\App\Http\Controllers\Fm@checkInodes");
    Route::post('/getLastInodesValue', "\App\Http\Controllers\Fm@getLastInodesValue");
    Route::get('/test', "\App\Http\Controllers\Fm@test");
    Route::get('/logout', function() {
        session()->flush();
        return redirect("/");
    });
});
